********************************
*
* Drush Usersets - A Drush command for creating, updating, and deleting users
* using data from a JSON or CSV data file.
*
********************************

Provides Drush commands for adding, updating, and deleting users from a CSV or JSON file.
This allows you to quickly and easily create users, update their information, block users,
and delete users, all by selecting a JSON or CSV data file. The main intended use case for
this command is the management of consistent sets of users, often development or testing users,
whose information can be maintained in a data file and then repeatedly created, deleted, or
otherwise acted upon as a group.

NOTE: This is not an actual Drupal module. It's only a drush command. For instructions on
installing commands, see the Commands section of the README.

********
* USAGE
********

Two commands are included: usersets-create (usc) and usersets-delete (usd). Basic usage:

Create users from file.json:
  drush usersets-create path/to/data/file.json

Create users from file.csv, or update them if they already exist (--force). Block all created or updated users:
  drush usc file.csv --type=CSV --force --block

Delete users in the file:
  drush usd file.json

Delete users from simple list:
  drush usd --names=amanda,jason,bob


To see all options and examples, run:

drush help usc
drush help usd


****************
* USAGE DETAILS
****************

1. Data file
----------------------

Begin by creating a data file. This can be in JSON or CSV formats. JSON is recommended.

Each item in the data file can have the following keys:

 - "username" (required): The username ('name' in the user object). This is used as the unique key
   to identify users.
 - "mail", "status", etc (optional): Any user object field can be passed. All keys are passed on to user_save()
   and will be saved accordingly.
 - "roles" (optional): Roles are handled separately. You can pass an array (in JSON), or a string separated by
   a forward slash (in CSV). Roles are only updated if they have changed.
 - "extra" (optional): This is a special key that gets ignored. Anything in it will *not* be saved to the user
   table. This is useful to pass on to the command's hooks. (For instance, use it to pass profile info and implement
   profile integration using a hook).
   
Here's an example of a data set in JSON:

{
  "jon" :
  {
    "username" : "jon",
    "mail" : "jon@awesomedrupal.com",
    "roles" : "developer",
    "favorite_color" : "blue",
  },
  "helen" :
  {
    "username" : "helen",
    "mail" : "helen@awesomedrupal.com",
    "roles" :
    [
      "developer",
      "qa",
      "ass kicker"
    ],
    "extra" :
    {
      "profile" :
      {
        "field_name_first" : "Helen",
        "field_body" : "She does not have a dog."
      }
    }
  }
}

Note that it's not necessary to have named objects like in the above example. You can pass
an array also, like this:

[
  [
    "username" : "jon",
    "mail" : "jon@awesomedrupal.com",
    "roles" : "developer",
    "favorite_color" : "blue",
  ],
  [
    "username" : "helen",
    "mail" : "helen@awesomedrupal.com",
    "roles" :
    [
      "developer",
      "qa",
      "ass kicker"
    ],
    "extra" :
    {
      "profile" :
      {
        "field_name_first" : "Helen",
        "field_body" : "She does not have a dog."
      }
    }
  ]
]

Note that in CSV it's of course not possible to pass arrays. Roles is a special case, where you can pass
in a forward slash separated string which will be parsed into an array. Other fields can only be strings.

2. Creating and updating users
-------------------------------




3. Deleting users
---------------

You can pass the same files above to the drush usersets-delete command. But only the username field is
used. All other fields are ignored. So you can also pass just a list of items with "username" fields.

For convenience, you can also just pass in a comma-separated list of user names directly into the command
using the --names option:

drush usd --names=amanda,jane,kevin

