<?php

/**
 * @file Drush command - create/update/delete users from CSV or JSON file.
 */

define('DRUSH_USERSETS_USER_ADDED', 1);
define('DRUSH_USERSETS_USER_UPDATED', 2);
define('DRUSH_USERSETS_USER_DELETED', 0);
define('DRUSH_USERSETS_USER_SKIPPED', 3);
define('DRUSH_USERSETS_USER_OP_ERROR', 4);

/**
 * Implementation of hook_drush_command().
 */
function drush_usersets_drush_command() {
  $items['usersets-create'] = array(
    'callback' => 'drush_usersets_create',
    'description' => 'Create users listed in a JSON or CSV file.',
    'aliases' => array('usc'),
    'arguments' => array(
      'file' => 'JSON or CSV file of users to create. JSON is assumed. Use --type to specify CSV.',
    ),
    'required-arguments' => TRUE,
    'examples' => array(
      'drush usersets-create --force users.json' =>
        'Create the users listed in users.json. If any of these users already exist in the database, update their information.',
      'drush usc /absolute/path/to/data.csv --type=csv' =>
        'Create the users listed in data.csv. If any of the users already exist, they will not be updated.',
    ),
    'options' => array(
      'type' => 'The type of input file (JSON or CSV). Defaults to JSON.',
      'force' => 'Update existing accounts if users already exist',
      'block' => 'Block the users being added or updated. If not set, users will be active.',
      'email' => 'Send email notifications to users about status changes and account creation. Off by default.',
    ),
  );
  $items['usersets-delete'] = array(
    'callback' => 'drush_usersets_delete',
    'description' => 'Delete users listed in a JSON or CSV file.',
    'aliases' => array('usd'),
    'arguments' => array(
      'file' => 'CSV or JSON file of users to create. The file must have an "mail" or "username" column/attribute, which will be used to identify users to delete. Other attributes will be ignored.',
    ),
    'required-arguments' => FALSE,
    'examples' => array(
      'drush usersets-delete users.json' =>
        'Delete the users listed in users.json.',
      'drush usd --names=bob,jen,arnie' =>
        'Delete the users bob, jen, and arnie.'
    ),
    'options' => array(
      'type' => 'The type of input file (JSON or CSV). Defaults to JSON.',
      'names' => 'Delete the users given by names to this option. If used, any file argument will be ignored. Value should be a comma-separated list of usernames. (i.e. --names=bob,jen,arnie)',
      'email' => 'Send email notifications to users about account deletion. Off by default.',
    ),
  );
  return $items;
}

/**
 * Callback for 'drush usersets-create'. Iterates over file and creates/updates users.
 */
function drush_usersets_create($file) {
  $type = drush_get_option('type', 'json');
  // Validate type option.
  if ($type != 'csv' && $type != 'json') {
    return drush_set_error('USERSETS_UNSUPPORTED_FILETYPE', dt('Only csv and json file types are supported'));
  }

  // Configure email settings for blocking/activating.
  global $conf;
  if (drush_get_option('email', 0) == 1) {
    $conf['user_mail_status_blocked_notify'] = TRUE;
    $conf['user_mail_status_activated_notify'] = TRUE;
  }
  else {
    $conf['user_mail_status_blocked_notify'] = FALSE;
    $conf['user_mail_status_activated_notify'] = FALSE;
  }

  // Loop over users file and process users.
  $get_item_function = '_drush_usersets_get_item_' . $type;
  $data = array();
  $updated = 0;
  $skipped = 0;
  $error = 0;
  while ($row = $get_item_function($file)) {
    // Add array for internal storage.
    $row['drush_usersets'] = array();
    $status = _drush_usersets_create_user($row);
    switch ($status) {
      case DRUSH_USERSETS_USER_ADDED:
      case DRUSH_USERSETS_USER_UPDATED:
        $updated++;
        break;
      case DRUSH_USERSETS_USER_SKIPPED:
        $skipped++;
        break;
      case DRUSH_USERSETS_USER_OP_ERROR:
        $error++;
        break;
    }
    $row['drush_usersets']['status'] = $status;
    $data[] = $row;
  }
  drush_log(format_plural($updated, 'Saved or updated 1 user', 'Saved or updated @count users.'), 'ok');
  if ($skipped) {
      drush_log(format_plural($skipped, '1 user was skipped because it already exist in the system. If you would like to update it, use the --force option.', '@count users were skipped because they already exist in the system. If you would like to update those users, use the --force option.'), 'warning');
  }
  if ($error) {
    drush_log(format_plural($error, '1 user was skipped because it was missing required fields (username, mail).', '@count users were skipped because they were missing required fields (username, mail).'), 'error');
  }
  
  module_invoke_all('drush_usersets_post_create', $data);
}

/**
 * Callback for 'drush usersets-delete'. Iterates over file and deletes users.
 */
function drush_usersets_delete($file = NULL) {
  $type = drush_get_option('type', 'json');
  // Validate type option.
  if ($type != 'csv' && $type != 'json') {
    return drush_set_error('USERSETS_UNSUPPORTED_FILETYPE', dt('Only csv and json file types are supported'));
  }

  if ($data = drush_get_option('names', '')) {
    $get_item_function = 'drush_usersets_get_item_from_names';
    if (empty($data) || $data == 1) {
      return drush_set_error('USERSETS_NO_DATA', dt('--names option used, but no usernames provided'));
    }
  }
  elseif ($file) {
    $get_item_function = '_drush_usersets_get_item_' . $type;
    $data = $file;
  }
  else {
    return drush_set_error('USERSETS_NO_DATA', dt('No input file or usernames were provided.'));
  }
  
  // Configure email settings for deleting.
  global $conf;
  if (drush_get_option('email', 0) == 1) {
    $conf['user_mail_status_deleted_notify'] = TRUE;
  }
  else {
    $conf['user_mail_status_deleted_notify'] = FALSE;
  }

  // Loop over users file and process users.
  $deleted = 0;
  $skipped = 0;
  $error = 0;
  while ($row = $get_item_function($data)) {
    $status = _drush_usersets_delete_user($row);
    switch ($status) {
      case DRUSH_USERSETS_USER_DELETED:
        $deleted++;
        break;
      case DRUSH_USERSETS_USER_SKIPPED:
        $skipped++;
        break;
      case DRUSH_USERSETS_USER_OP_ERROR:
        $error++;
        break;
    }
  }
  drush_log(format_plural($deleted, 'Deleted 1 user', 'Deleted @count users.'), 'ok');
  if ($skipped) {
      drush_log(format_plural($skipped, "1 user was skipped because it doesn't exist in the system.", "@count users were skipped because they don't exist in the system."), 'warning');
  }
  if ($error) {
    drush_log(format_plural($error, '1 user was skipped because it was missing required fields (username or mail).', '@count users were skipped because they were missing required fields (username or mail).'), 'error');
  }
}

/**
 * Creates or updates a user.
 *
 * @param $row An associative array of user fields. Must contain keys "username" and "mail".
 */
function _drush_usersets_create_user(&$row) {
  // Make sure we have required fields
  if (!isset($row['username']) && !isset($row['mail'])) {
    drush_set_error('USERSETS_ADD_REQUIRED_FIELDS_MISSING', dt("Can't add user: username and mail fields required"));
    return DRUSH_USERSETS_USER_OP_ERROR;
  }

  $account_details = array(
    'name' => $row['username'],
    'mail' => $row['mail'],
  );

  // Force a blocked status if --block option was given.
  if (drush_get_option('block', FALSE)) {
    $account_details['status'] = 0;
  }

  // Check if there's already such an account
  if ($existing_user = user_load(array('name' => $row['username']))) {
    if (drush_get_option('force', 0)) {
      // Add the rest of the $row to the account details array. This way custom fields from the
      // file can be added to the user object. They won't overwrite the fields we've manually set here.
      $account_details = array_merge($row, $account_details);
      // Unset the special keys that do not go into account
      unset($account_details['username']);
      unset($account_details['extra']);

      // Reassign user roles, if necessary.
      if (isset($row['roles'])) {
        // First, remove authenticated user role so it doesn't mess up comparison.
        unset($existing_user->roles[DRUPAL_AUTHENTICATED_RID]);
        $roles = _drush_usersets_get_role_ids($row['roles']);
        if ($roles != $existing_user->roles) {
          $account_details['roles'] = $roles;
        }
        else {
          // No need to update roles.
          unset($account_details['roles']);
        }
      }

      // Add an indicator that this user was touched by this command.
      $account_details['drush_usersets'] = TRUE;
      // Update existing user
      $updated_user = user_save($existing_user, $account_details);
      if (isset($updated_user->uid)) {
        drush_log(dt('Updated user @name (uid @uid)', array('@name' => $row['username'], '@uid' => $updated_user->uid)), 'success');
        $status = DRUSH_USERSETS_USER_UPDATED;
        $row['drush_usersets']['uid'] = $updated_user->uid;
      }
      else {
        drush_set_error('USERSETS_UPDATE_FAILURE', dt('Failed to update user @user', array('@user' => $row['username'])));
        $status = DRUSH_USERSETS_USER_OP_ERROR;
      }
    }
    else {
      drush_log(dt('Skipping user @name.', array('@name' => $row['username'])), 'warning');
      $status = DRUSH_USERSETS_USER_SKIPPED;
      $row['drush_usersets']['uid'] = $existing_user->uid;
    }
  }
  else {
    // First manually set some fields for the new user.
    $account_details['roles'] = _drush_usersets_get_role_ids($row['roles']);
    $account_details['init'] = $row['mail'];
    $account_details['pass'] = user_password(16);

    // Add the rest of the $row to the account details array. This way custom fields from the
    // file can be added to the user object. They won't overwrite the fields we've manually set here.
    $account_details = array_merge($row, $account_details);
    // Unset the special keys that do not go into account
    unset($account_details['username']);
    unset($account_details['extra']);

    // Add an indicator that this user was touched by this command.
    $account_details['drush_usersets'] = TRUE;
    // Create new user
    $new_user = user_save(NULL, $account_details);
    if (isset($new_user->uid)) {
      drush_log(dt('Created new user @name (uid @uid)', array('@name' => $row['username'], '@uid' => $new_user->uid)), 'success');
      if (drush_get_option('email', 0) == 1) {
        _user_mail_notify('register_admin_created', $new_user);
      }
    }
    $status = DRUSH_USERSETS_USER_ADDED;
    $row['drush_usersets']['uid'] = $new_user->uid;
  }
  return $status;
}

/**
 * Deletes a user.
 *
 * @param $row An associative array of user fields. Must contain at least one
 *             of the keys "username", "mail" to identify the user.
 */
function _drush_usersets_delete_user($row) {
  if (isset($row['username'])) {
    $account = user_load(array('name' => $row['username']));
    if (!$account) {
      drush_log(dt('Skipped user @username. User not found', array('@username' => $row['username'])), 'warning');
      return DRUSH_USERSETS_USER_SKIPPED;
    }
  }
  elseif (isset($row['mail'])) {
    $account = user_load(array('mail' => $row['mail']));
    if (!$account) {
      drush_log(dt('Skipped user with email @email. User not found', array('@email' => $row['mail'])), 'warning');
      return DRUSH_USERSETS_USER_SKIPPED;
    }
  }
  else {
    drush_set_error('USERSETS_DELETE_REQUIRED_FIELDS_MISSING', dt("Can't delete user. Please specify the username or mail field."));
    return DRUSH_USERSETS_USER_OP_ERROR;
  }
  // Delete the user.
  if ($account->uid != 1) {
    user_delete(array(), $account->uid);
    // Make sure the user was deleted.
    if ($deleted_account = user_load(array('uid' => $account->uid))) {
      drush_set_error('USERSETS_DELETE_FAILURE', dt("Could not delete the user."));
      $status = DRUSH_USERSETS_USER_OP_ERROR;
    }
    else {
      drush_log(dt('Deleted user @name', array('@name' => $account->name)), 'ok');
      $status = DRUSH_USERSETS_USER_DELETED;
    }
  }
  else {
    drush_log(dt('Skipped user 1'));
    $status = DRUSH_USERSETS_USER_SKIPPED;
  }
  return $status;
}

/**
 * Get the next user from the file - JSON edition.
 */
function _drush_usersets_get_item_json($filepath) {
  static $data;
  if (!isset($data)) {
    $file_content = file_get_contents($filepath);
    if (!$file_content) {
      return drush_set_error('USERSETS_FILE_OPEN_FAILURE', dt('Unable to open file: @filepath', array('@filepath' => $filepath)));
    }
    $data = drush_json_decode($file_content);
    if (!$data) {
      return drush_set_error('USERSETS_JSON_UNREADABLE', dt('Unable to decode the json file. Check for syntax errors.'));
    }
  }
  return array_pop($data);
}

/**
 * Get the next user from the file - CSV edition.
 */
function _drush_usersets_get_item_csv($filepath) {
  static $file = FALSE;
  static $headers = NULL;
  $retval = FALSE;
  if (!$file) {
    if (($file = @fopen($filepath, "r")) == FALSE) {
      return drush_set_error('USERSETS_FILE_OPEN_FAILURE', dt('Unable to open file: @filepath', array('@filepath' => $filepath)));
    }
    else {
      $headers = fgetcsv($file);
    }
  }
  // we have a file_handle, and column headers, so get data-row.
  if ($row = fgetcsv($file)) {
    $retval = array_combine($headers , $row);
  }
  elseif ($file) {
    // Otherwise close file.
    @fclose($file);
    $file = FALSE;
  }
  return $retval;
}

/**
 * Mimics the behavior of the functions that load data from files, but instead
 * gets the data from a string of comma-separated usernames.
 */
function drush_usersets_get_item_from_names($names) {
  static $data;
  if ($data === NULL) {
    $data = explode(',', $names);
  }
  if ($next = array_pop($data)) {
    return array('username' => $next);
  }
}

/**
 * Returns an array mapping role ids to role names that can be passed to user_save.
 *
 * @param $role_names_string A string or array of role names
 * @param $separator The separator for the role name string.
 */
function _drush_usersets_get_role_ids($role_names, $separator = '/') {
  if (!is_array($role_names)) {
    $role_names = explode($separator, $role_names);
  }
  
  $roles = array();
  if (!empty($role_names)) {
    $system_roles = user_roles(TRUE);
    $system_roles = array_flip($system_roles);
    foreach ($role_names as $role_name) {
      if (isset($system_roles[$role_name])) {
        $roles[$system_roles[$role_name]] = $role_name;
      }
      else {
        drush_log(dt("Can't assign role '@role'. Role not found.", array('@role' => $role_name)), 'error');
      }
    }
  }
  return $roles;
}
